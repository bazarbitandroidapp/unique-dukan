package com.pureweblopment.uniquedukan.PaymentGateway;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pureweblopment.uniquedukan.Activity.MainActivity;
import com.pureweblopment.uniquedukan.Fragment.PaymentFragment;
import com.pureweblopment.uniquedukan.R;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RazorpayScreen.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RazorpayScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RazorpayScreen extends Fragment implements PaymentResultListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    Bundle bundle = new Bundle();

    public RazorpayScreen() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RazorpayScreen.
     */
    // TODO: Rename and change types and number of parameters
    public static RazorpayScreen newInstance(String param1, String param2) {
        RazorpayScreen fragment = new RazorpayScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_razorpay_screen, container, false);
        bundle = getArguments();
        startPayment(bundle);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void startPayment(Bundle bundle) {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */

        String strName = bundle.getString("firstname");
        String strBookingInfo = "";
        String strAmount = bundle.getString("amount");
        String strEmail = bundle.getString("emailid");
        String strPhone = bundle.getString("phone");
        String strCurrency = bundle.getString("currency");

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", strName);
            options.put("description", strBookingInfo);
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", strCurrency);
            options.put("amount", strAmount);

            JSONObject preFill = new JSONObject();
            preFill.put("email", strEmail);
            preFill.put("contact", strPhone);

            options.put("prefill", preFill);

            co.open(getActivity(), options);
        } catch (Exception e) {
            Toast.makeText(getContext(), "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        MainActivity.checkback = false;
        String status = "Transaction Successfully!";
        mListener = (OnFragmentInteractionListener) getContext();
        mListener.gotoOrderSuccess("2", status);
    }

    @Override
    public void onPaymentError(int i, String s) {
        MainActivity.checkback = true;
        String status = "Transaction Cancelled!";
        mListener = (OnFragmentInteractionListener) getContext();
        mListener.gotoOrderSuccess("2", status);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void gotoOrderSuccess(String payment_method, String urlStatus);
    }
}
